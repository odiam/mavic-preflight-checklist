# Mavic Pro Pre-flight checklist

### 1. Check ambientali

1. Ispezione visiva zona di volo per presenza ostacoli
2. Scelta del punto di decollo/atterraggio, lontano da ostacoli in ogni direzione per almeno 10 metri

### 2. Check preliminari

1. Controllo batteria drone (premere una volta tasto accensione)
2. Controllo batteria RC (premere una volta tasto accensione)
3. Apertura drone
4. Se necessario, aggancio carrello
5. Ispezione visiva status eliche
6. Ispezione mobilità rotori
7. Ispezione aggancio corretto della batteria

### 3. Check camera

1. Rimuovere copertura camera
2. Rimuovere blocco gimbal
3. Se si intende fare riprese/foto, rimozione cupola trasparente ed installazione protezione esterna.
4. Se presente, ispezione visiva aggancio filtro ND sulla camera

### 4. Check elettronici

1. Accensione drone (premere una volta tasto accensione, premere seconda volta e tenere premuto)
2. Accensione RC (premere una volta tasto accensione, premere seconda volta e tenere premuto), collegamento device esterno, controllo connessione drone
3. Controllo antenne: verticali, tilt forward in direzione del drone
4. Click barra in alto nell'applicazione -> Aircraft Status -> tutto normale
5. Main Control Settings -> controllo Max Flight Altitude
6. Main Control Settings -> controllo Max Distance
7. Main Control Settings -> Advanced -> Sensors -> controllo cambiamento IMU max +- 0.1
8. Main Control Settings -> Advanced -> Sensors -> controllo compass MOD fra 1400 e 1600
9. Main Control Settings -> Advanced -> controllo comportamento Remote Controller Signal Lost: Hover per indoor, Return to Home per outdoor
10. Visual Navigation Settings -> Controllo settaggi per volo visuale

### 5. Take-off

1. Luce verde intermittenza lenta = safe to fly GPS, P-mode/S-mode, Downward vision system
2. Attendere "Ready to Go (GPS)" su app
3. Assicurarsi di avere almeno 8 satelliti collegati (barra in alto nell'app)
4. Assicurarsi che l'Home Point è registrato con successo (indicazione visuale e audio), altrimenti Main Control Settings -> Home Point Settings -> registrazione home point manuale
5. Avvio motori
6. Ispezione uditiva motori
7. Take off & hover
8. Test spostamento lento sui 3 assi
9. Go
