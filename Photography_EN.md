# Mavic Pro Photography checklist

### 1. Main Control Settings

1. Advanced -> EXP: 0.25, 0.20, 0.25
2. Advanced -> Gain -> Yaw: ~80%

### 2. Remote Control Settings

1. C1: center focus
2. 5D -> up: camera forward/down, down: metering/focusing switch, left: zoom in, right: zoom out

### 3. Gimbal Settings

1. Advanced -> gimbal speed: 8
2. Advanced -> gimbal tilt limit: 30° extensible
3. Advanced -> gimbal pan synchronous follow: on
4. Advanced -> gimbal stop/start buffering: 20

### 4. Camera Settings (sliders)

1. Camera icon -> Video size: 3840x2160, 30fps
2. Camera icon -> Video format: MOV
3. NTSC/PAL: NTSC (suited for 30fps)
4. Camera icon -> White balance: depending on sunlight conditions
5. Camera icon -> Style: Custom, +1, -1, 0
6. Camera icon -> Color: D-log
7. Gear icon -> Histrogram: on
8. Gear icon -> Front LEDs Auto Turn Off
9. Gear icon -> Center Points
10. Gear icon -> Anti-flicker: 50Hz
11. Gear icon -> File Index Mode: Continous
12. Shutter icon -> Manual
13. Shutter icon -> ISO: 100 (normal light conditions) - 400 (low light conditions)
14. Shutter icon -> Shutter: 60 (fps x 2) NB: ND filter needed
