# Mavic Pro Pre-flight checklist

### 1. Environment checks

1. Visual inspection of flight area for obstacles
2. Choose take-off/landing point, far from any obstacle for at least 10 meters in all directions

### 2. Preliminary checks

1. Drone battery check (press on/off switch once)
2. RC battery check (press on/off switch once)
3. Unfold drone
4. If needed, attach landing gear
5. Visual inspection of propellers
6. Visual inspection of engines and their mobility
7. Battery docking inspection, safely connected to the drone

### 3. Camera checks

1. Remove camera cover
2. Remove gimbal block
3. If photo/video shooting flight, remove transparent camera dome
4. If present, visual inspection of ND filter attachment

### 4. Electronic checks

1. Switch on drone (press on/off switch once, press again and keep it pressed)
2. Switch on RC (press on/off switch once, press again and keep it pressed)
3. Antenna check: vertical, tilted forward towards drone
4. Click top bar on app -> Airfract status -> all normal
5. Main Control Settings -> Max Flight Altitude check
6. Main Control Settings -> Max Distance check
7. Main Control Settings -> Advanced -> Sensors -> check IMU range max +- 0.1
8. Main Control Settings -> Advanced -> Sensors -> check compass MOD between 1400 and 1600
9. Main Control Settings -> Advanced -> Remote Controller Signal Lost behaviour check: Hover for indoor, Return to Home for outdoor
10. Visual Navigation Settings -> Check every setting

### 5. Take-off

1. Slowly blinking green light = safe to fly GPS, P-mode/S-mode, Downward vision system
2. Wait for "Ready to go (GPS)" on the app
3. Make sure to have at least 8 satellites connected (top bar in the app)
4. Make sure the Home Point is correctly saved (visual and audio cues). If not, Main Control Settings -> Home Point Settings -> do it manually
5. Start engines
6. Audio inspection of engines
7. Take off & hover
8. 3-axis test, slow movement in all directions
9. Good to go
